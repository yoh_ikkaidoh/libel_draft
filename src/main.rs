use std::fs::File;
use std::io::{Read, Write};
use std::path::Path;

fn get_sentences(file: &str) -> std::io::Result<Vec<String>> {
    let mut file = File::open(&Path::new(file))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    Ok(input.split_terminator('。').map(|e| e.to_string())
       .filter(|e| !e.starts_with("#") && !e.trim().is_empty() && !e.starts_with('\u{FEFF}'))
       .map(|mut e|{e.push('。');e})
       .collect())
}

fn split_by_period(file: &str, cs: usize) -> std::io::Result<Vec<String>> {
    let sentences = get_sentences(&file)?;
    let mut chunks = vec![];
    let mut buffer = String::new();
    for sentence in sentences {
        buffer.push_str(&sentence);
        if buffer.len() > cs {
            chunks.push(buffer.clone());
            buffer.clear();
        }
    }
    chunks.push(buffer);
    Ok(chunks)
}

fn main() -> std::io::Result<()> {
    let args: Vec<_> = std::env::args().collect();
    let chunk_size: usize = args[2].parse().unwrap();
    let output_root = &args[3];
    let chunks = split_by_period(&args[1], chunk_size)?;
    for (index, chunk) in chunks.into_iter().enumerate() {
        let mut output = File::create(Path::new(&format!("{}/{}.txt", output_root, index)))?;
        writeln!(&mut output, "{}", chunk)?;
    }
    Ok(())
}
