const node_radius = 25;
const svg = d3.select(".gittree")
      .append("svg")
      .classed("gittree expand", true);

const graph_svg = svg
      .append("g");

const edge_svg = graph_svg
      .append("g")
      .attr("class", "links");

const node_svg = graph_svg
      .append("g")
      .attr("class","nodes");

const text_card = d3.select(".textcard");
const text_card_main = text_card.select(".main");
const text_card_header = text_card.select(".header");
const text_card_footer = text_card.select(".footer");

const line = link => {
    const len = link.length;
    return d3.line()
        .curve(d3.curveCatmullRom)
        .x(d => d.x)
        .y(d => d.y)(link);
};

const line_squish = link => {
    const len = link.length;
    return d3.line()
        .curve(d3.curveCatmullRom)
        .x(d => d.x/3.5)
        .y(d => d.y)(link);
};
const expandNode = (flag, text_root, dag) =>
      d3.text(`${text_root}/${flag.id}.txt`)
      .then(text => {
          svg
              .classed("expand",false)
              .classed("squish",true);
          const nodes = node_svg
                .selectAll(".node")
                .data(dag.descendants())
                .classed("focused", d=>d.id == flag.id);
          
          const updateCard = (_) => {
              console.log("UPDATE");
              text_card_main.text(text);
              text_card_footer.selectAll("*")
                  .remove();
              const message = `This is the ${flag.id}-th paragraph on TEST-branch`;
              text_card_header
                  .selectAll("*")
                  .remove();
              text_card_header
                  .html(generateHeaderHtml(message));
              text_card_footer
                  .append("div")
                  .text("Notes about the author, the paragraph, or the node.");
              text_card_footer
                  .append("div")
                  .html(`<i class="fas fa-share-alt-square"></i>`);
              text_card_footer
                  .append("div")
                  .html(`<i class="fas fa-download"></i>`);
              text_card_header.select(".close-button")
                  .on("click",(flag) =>  closeNode(flag,dag));
          };
          let timer = d3.transition().duration(500)
              .on("end",(_) => updateCard());
          let timer2 = timer.transition().duration(500);
          let timer3 = timer2.transition().duration(250);
          if ( text_card.classed("active")){
              text_card
                  .transition(timer)
                  .style("opacity",0);
              text_card
                  .transition(timer2)
                  .style("opacity",1);
          }else{
             text_card.classed("active",true);
             text_card
                  .transition()
                  .style("opacity",1);
              const edges = edge_svg
                    .selectAll(".link")
                    .data(dag.links())
                    .transition(timer)
                    .attr("d",link => line_squish(link.data.points));
              const nodes = node_svg
                    .selectAll(".node")
                    .data(dag.descendants())
                    .transition(timer)
                    .attr("r", node_radius)
                    .attr("cx",d => d.x/3.5);
              const labels = node_svg
                    .selectAll(".label")
                    .data(dag.descendants())
                    .transition(timer)
                    .attr("x", d=>d.x/3.5);
          }
      })
      .then(ok => console.log("OK"),
            why => console.log(why));

const closeNode = (flag, dag) => {
    svg.classed("expand",true)
        .classed("squish", false);
    text_card.classed("active",false);
    text_card_header.html("");
    text_card_footer.selectAll("*").remove();
    text_card_main.text("");
    let timer = d3.transition().duration(1000);
    node_svg.data(dag.descendants())
        .selectAll(".node")
        .classed("focused",false);
    const edges = edge_svg
          .selectAll(".link")
          .data(dag.links())
          .transition(timer)
          .attr("d",link => line(link.data.points));
    const nodes = node_svg
          .selectAll(".node")
          .data(dag.descendants())
          .transition(timer)
          .attr("r", node_radius)
          .attr("cx",d => d.x);
    const labels = node_svg
          .selectAll(".label")
          .data(dag.descendants())
          .transition(timer)
          .attr("x", d=>d.x);
};




const generateHeaderHtml = (description) => {
    return `<div class = "description">${description}</div>
      <svg width = "25" height = "25" class = "close-button">
        <rect width = "25" height = "25" class = "rect">
        </rect>
        <line x1="5" x2="20" y1="5" y2="20" class="line"></line>
        <line x1="5" x2="20" y1="20" y2="5" class="line"></line>
      </svg>`;
};

const plot_graph = (root, graph_path, text_root) =>
      d3.json(graph_path).then(graph => {
          const width = parseInt(svg.style("width"));
          const height = parseInt(svg.style("height"));
          const margin = Math.max(width,height)/10;
          graph_svg.attr("transform",`translate(${margin/2},${margin/2})`);
          let dag = d3.dagStratify()(graph);
          d3.sugiyama()
              .size([width-margin,height-margin])(dag);
          const edges = edge_svg
                .selectAll(".link")
                .data(dag.links())
                .enter();
          edges
              .append("path")
              .attr("class", "link")
              .attr("d",link => line(link.data.points));
          const nodes = node_svg
                .selectAll(".node")
                .data(dag.descendants())
                .enter();
          nodes.append("circle")
              .attr("class","node")
              .attr("r", node_radius)
              .attr("cx",d => d.x)
              .attr("cy", d => d.y)
              .on("click",(flag) => expandNode(flag,text_root, dag));
          const labels = node_svg
                .selectAll(".label")
                .data(dag.descendants())
                .enter();
          nodes.append("text")
              .attr("class","label")
              .attr("x", d=>d.x)
              .attr("y", d=>d.y)
              .text(d => d.id);
      })
      .then(ok => console.log("OK"), ng => console.log(ng));
